<?php include 'header.php'; ?>  
        <!-- Hidden Bar -->
        <section class="hidden-bar right-align">
            
            <div class="hidden-bar-closer">
                <button class="btn"><i class="fa fa-close"></i></button>
            </div>
            <!-- Hidden Bar Wrapper -->
            <div class="hidden-bar-wrapper">
            
                <!-- .logo -->
                <div class="logo text-center">
                    <a href="index-2.html"><img src="images/logo-2.png" alt=""></a>         
                </div><!-- /.logo -->
                
                <!-- .Side-menu -->
                <div class="side-menu">
                <!-- .navigation -->
                    <ul class="navigation">
                        <li class="current dropdown"><a href="index-2.html">Home</a>
                            <ul class="submenu">
                                <li><a href="index-2.html">Home Page Style I</a></li>
                                <li><a href="index-3.html">Home Page Style II</a></li>
                                <li><a href="index-4.html">Home Page Style III</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="causes.html">Causes</a>
                            <ul class="submenu">
                                <li><a href="causes.html">Our Team</a></li>
                                <li><a href="cause-single.html">Causes Detail</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="gallery-style-one.html">Gallery</a>
                            <ul class="submenu">
                                <li><a href="gallery-style-one.html">Gallery Style I</a></li>
                                <li><a href="gallery-style-two.html">Gallery Style II</a></li>
                                <li><a href="gallery-style-three.html">Gallery Style III</a></li>
                                <li><a href="gallery-style-four.html">Gallery Style IV</a></li>
                                <li><a href="gallery-style-five.html">Gallery Style V</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Pages</a>
                            <ul class="submenu">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-services.html">Our Services</a></li>
                                <li><a href="blog-list.html">Blog List View</a></li>
                                <li><a href="blog-grid.html">Blog Grid View</a></li>
                                <li><a href="blog-single.html">Blog Details</a></li>
                                <li><a href="our-stories.html">Welfare Stories</a></li>
                                <li><a href="event-calendar.html">Event Calendar</a></li>
                                <li><a href="event-details.html">Event Details</a></li>
                                <li><a href="error-page.html">404 Error Page</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="shop.html">Shop</a>
                            <ul class="submenu">
                                <li><a href="shop.html">Shop</a></li>
                                <li><a href="shop-single.html">Shop Details</a></li>
                                <li><a href="shopping-cart.html">Shopping Cart</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.html">Contact</a></li>
                    </ul>
                </div><!-- /.Side-menu -->
            
                <div class="social-icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            
            </div><!-- / Hidden Bar Wrapper -->
        </section><!-- / Hidden Bar -->
        
        
        <!--Page Title Section-->
        <section class="page-title-section" style="background-image:url(images/background/page-title-1.jpg);">
            <div class="auto-container">
                <div class="page-title">
                    
                    <h2>Join Us</h2>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                        <!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active">Join Us</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!--All Causes Section-->
        <section class="recent-causes-section all-causes-section">
            <div class="auto-container">
                
                <div class="row clearfix">
                  
                  <div class="contact-form-container wow fadeInUp animated" data-wow-delay="0ms" data-wow-duration="2500ms" style="visibility: visible; animation-duration: 2500ms; animation-delay: 0ms; animation-name: fadeInUp;">
                        <form method="post" action="#" novalidate="novalidate" _lpchecked="1">
                            
                            <div class="row clearfix">
                                <div class="column col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="username" value="" placeholder="Full Name">
                                    </div>
                                </div>
                                
                                <div class="column col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="email" name="email" value="" placeholder="Email Address">
                                    </div>
                                </div>
                                
                                <div class="column col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="subject" value="" placeholder="Subject">
                                    </div>
                                </div>
                                
                                <div class="column col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <textarea name="message" placeholder="Detail"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12 col-xs-12">
                                    <div class="text-center"><button type="submit" name="submit" class="theme-btn btn-style-one">Join Now</button></div>
                                </div>
                            
                            </div>
                            
                        </form>
                    </div>  
                  
                </div>
                
                
            </div>
        </section>
        
    <?php include 'footer.php'; ?>  