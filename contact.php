<?php include 'header.php'; ?>  
        <section class="page-title-section" style="background-image:url(images/background/page-title-1.jpg);">
            <div class="auto-container">
            	<div class="page-title">                	
                    <h2>Contact Us</h2>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                    	<!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Contact Us</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!--Map Section-->
        <section class="map-section">
        	<div class="auto-container">
                
                <!--Google Map Container-->
                <div class="map-box" id="map1">
                
                </div>
                
        	</div>
        </section>
        
        
        <!--Contact Section-->
    	<section class="contact-section">
            <div class="auto-container">
                <div class="inner-container">
                            
                    <div class="section-title">
                      
                        <h2 style="color: #f25f43 ">Get In Touch</h2>
                    
                    </div>
                    
                    <!--Contact Info-->
                    <div class="contact-info row clearfix">
                        <!--Info COlumn-->
                        <div class="info-column col-lg-4 col-md-6 col-xs-12">
                            <div class="inner-box">
                                <div class="icon"><span class="fa fa-home"></span></div>
                                <h4>ADDRESS:</h4>
                                Village- khushalgarhi,  <br> Post-Talivnagar,
                               <br>Aligarh (U.P.)
                               <br>202127
                            </div>
                        </div>
                        
                        <!--Info COlumn-->
                        <div class="info-column col-lg-4 col-md-6 col-xs-12">
                            <div class="inner-box">
                                <div class="icon"><span class="fa fa-envelope"></span></div>
                                <h4>Email Address:</h4>
                                info@janjeevansanstha.com
                            
                            </div>
                        </div>
                        
                        <!--Info COlumn-->
                        <div class="info-column col-lg-4 col-md-6 col-xs-12">
                            <div class="inner-box">
                                <div class="icon"><span class="fa fa-phone"></span></div>
                                <h4>Phone No:</h4>
                               +91-7906740833<br>
                               +91 8006762604
                            </div>
                        </div>
                    </div>
                    
                    <!--Form Container-->
                    <div class="contact-form-container wow fadeInUp" data-wow-delay="0ms" data-wow-duration="2500ms">
                        <form method="post" action="sendeMail.php" id="contact-form">
                            
                            <div class="row clearfix">
                                <div class="column col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="username" value="" placeholder="Full Name">
                                    </div>
                                </div>
                                
                                <div class="column col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="email" name="email" value="" placeholder="Email Address">
                                    </div>
                                </div>
                                
                                <div class="column col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" name="subject" value="" placeholder="Subject">
                                    </div>
                                </div>
                                
                                <div class="column col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <textarea name="message" placeholder="Detail"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-12 col-xs-12">
                                    <div class="text-center"><button type="submit" name="submit" class="theme-btn btn-style-one">Contact Now</button></div>
                                </div>
                            
                            </div>
                            
                        </form>
                    </div>
                    
                </div>
            </div>
        </section>
        
            
      
                <!--Copyright-->
                
            </div>
        </div>
        
    </footer>  

    </div><!--Page Wrapper End-->
</div>
<!--page-outer-container-->


<script>
      function initMap() {
        var aligarh = {lat: 27.8974, lng: 78.0880};
        /*var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: aligarh
        });*/

        var map1 = new google.maps.Map(document.getElementById('map1'), {
          zoom: 10,
          center: aligarh
        });

      /*  var marker = new google.maps.Marker({
          position: aligarh,
          map: map
        });*/

        var marker1 = new google.maps.Marker({
          position: aligarh,
          map: map1
        });
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh8qtBfkcIAeJ-yC4aIl3ki_Adt7u57Cg&callback=initMap">
    </script>



<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>
        
    
	</div><!--Page Wrapper End-->
</div>
<!--page-outer-container-->


<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/validate.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/owl.js"></script>
<!-- <script src="http://maps.google.com/maps/api/js"></script> -->
<!-- <script src="js/googlemaps.js"></script> -->
<script src="js/wow.js"></script>
<script src="js/script.js"></script>
</body>

<!-- Mirrored from alone.themexriver.com/alone/contact-us.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Sep 2017 17:48:07 GMT -->
</html>
