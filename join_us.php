<?php include 'header.php'; ?>      
        
        <!--Page Title Section-->
        <section class="page-title-section" style="background-image:url(images/background/page-title-1.jpg);">
            <div class="auto-container">
            	<div class="page-title">
                	<h3>Change These Changes &amp; Everything.</h3>
                    <h2>Our Stories</h2>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                    	<!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Our Stories</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!--All Stories Section-->
        <section class="default-stories-section all-stories-section">
        	<div class="auto-container">
                
            	<div class="row clearfix">
       				
                    <!--Column-->
               
                    
                    <!--Column-->
                    <div class="column col-md-6 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    	<!--Default Story Column-->
                        <article class="default-story-column">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/story-image-2.jpg" alt=""></figure>
                                <div class="overlay-box">
                                    <div class="bg-box">
                                        <div class="content-box">
                                            <h2>Fuel-efficient cookstoves...</h2>
                                            <ul class="info clearfix">
                                                <li>Donation So Far: <span class="amount">$15,000</span></li>
                                                <li class="text-uppercase">in South Africa</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="over-link"></a>
                            </div>
                        </article>
                    </div>
                    
                    <!--Column-->
                    <div class="column col-md-6 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    	<!--Default Story Column-->
                        <article class="default-story-column">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/story-image-4.jpg" alt=""></figure>
                                <div class="overlay-box">
                                    <div class="bg-box">
                                        <div class="content-box">
                                            <h2>Feeding West Africa: Agenda...</h2>
                                            <ul class="info clearfix">
                                                <li>Donation So Far: <span class="amount">$15,000</span></li>
                                                <li class="text-uppercase">in South Africa</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="over-link"></a>
                            </div>
                        </article>
                    </div>
                    
                    
                    <!--Column-->
                    <div class="column col-md-6 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    	<!--Default Story Column-->
                        <article class="default-story-column">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/story-image-5.jpg" alt=""></figure>
                                <div class="overlay-box">
                                    <div class="bg-box">
                                        <div class="content-box">
                                            <h2>Building financial capability</h2>
                                            <ul class="info clearfix">
                                                <li>Donation So Far: <span class="amount">$15,000</span></li>
                                                <li class="text-uppercase">in South Africa</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="over-link"></a>
                            </div>
                        </article>
                    </div>
                    
                    
                    <!--Column-->
                
                    
                    <!--Column-->
                    <div class="column col-md-6 col-sm-12 col-xs-12 wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                    	<!--Default Story Column-->
                        <article class="default-story-column">
                            <div class="inner-box">
                                <figure class="image-box"><img src="images/resource/story-image-7.jpg" alt=""></figure>
                                <div class="overlay-box">
                                    <div class="bg-box">
                                        <div class="content-box">
                                            <h2>Higher Salaries Can Worsen</h2>
                                            <ul class="info clearfix">
                                                <li>Donation So Far: <span class="amount">$15,000</span></li>
                                                <li class="text-uppercase">in South Africa</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="over-link"></a>
                            </div>
                        </article>
                    </div>
                    
                    
       			</div>
                
                <!-- Styled Pagination -->
                <nav class="styled-pagination text-center padd-top-50">
                    <ul>
                        <li><a class="prev" href="#"><span class="fa fa-angle-double-left"></span></a></li>
                        <li><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#" class="active">03</a></li>
                        <li>..........</li>
                        <li><a href="#">08</a></li>
                        <li><a href="#">09</a></li>
                        <li><a class="next" href="#"><span class="fa fa-angle-double-right"></span></a></li>
                    </ul>
                </nav>
                
        	</div>
        </section>
        
        
        
            
       <?php include 'footer.php'; ?>      