<?php include 'header.php'; ?>  
        
        <!-- Hidden Bar -->
        <section class="hidden-bar right-align">
            
            <div class="hidden-bar-closer">
                <button class="btn"><i class="fa fa-close"></i></button>
            </div>
            <!-- Hidden Bar Wrapper -->
            <div class="hidden-bar-wrapper">
            
                <!-- .logo -->
                <div class="logo text-center">
                    <a href="index-2.html"><img src="images/logo-2.png" alt=""></a>			
                </div><!-- /.logo -->
                
                <!-- .Side-menu -->
                <div class="side-menu">
                <!-- .navigation -->
                    <ul class="navigation">
                        <li class="current dropdown"><a href="index-2.html">Home</a>
                            <ul class="submenu">
                                <li><a href="index-2.html">Home Page Style I</a></li>
                                <li><a href="index-3.html">Home Page Style II</a></li>
                                <li><a href="index-4.html">Home Page Style III</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="causes.html">Causes</a>
                            <ul class="submenu">
                                <li><a href="causes.html">Our Causes</a></li>
                                <li><a href="cause-single.html">Causes Detail</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="gallery-style-one.html">Gallery</a>
                            <ul class="submenu">
                                <li><a href="gallery-style-one.html">Gallery Style I</a></li>
                                <li><a href="gallery-style-two.html">Gallery Style II</a></li>
                                <li><a href="gallery-style-three.html">Gallery Style III</a></li>
                                <li><a href="gallery-style-four.html">Gallery Style IV</a></li>
                                <li><a href="gallery-style-five.html">Gallery Style V</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Pages</a>
                            <ul class="submenu">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-services.html">Our Services</a></li>
                                <li><a href="blog-list.html">Blog List View</a></li>
                                <li><a href="blog-grid.html">Blog Grid View</a></li>
                                <li><a href="blog-single.html">Blog Details</a></li>
                                <li><a href="our-stories.html">Welfare Stories</a></li>
                                <li><a href="event-calendar.html">Event Calendar</a></li>
                                <li><a href="event-details.html">Event Details</a></li>
                                <li><a href="error-page.html">404 Error Page</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="shop.html">Shop</a>
                            <ul class="submenu">
                                <li><a href="shop.html">Shop</a></li>
                                <li><a href="shop-single.html">Shop Details</a></li>
                                <li><a href="shopping-cart.html">Shopping Cart</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.html">Contact</a></li>
                    </ul>
                </div><!-- /.Side-menu -->
            
                <div class="social-icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            
            </div><!-- / Hidden Bar Wrapper -->
        </section><!-- / Hidden Bar -->
        
        
        <!--Page Title Section-->
        <section class="page-title-section" style="background-image:url(images/background/page-title-2.png);">
            <div class="auto-container">
            	<div class="page-title">                	
                    <h2>Donate</h2>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                    	<!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active">Donate</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>

        <!--Gallery Section / Masonry Gallery-->
        <section class="gallery-section mixed-gallery-section sortable-masonry">
            <div class="auto-container">
              
                
                <!--Items Container-->
                <div class="row items-container clearfix">
                    



                    
                 
                    
                    
                    <!--Column-->
                    <div class="column masonry-column all scholarship education col-md-6 col-sm-6 col-xs-12">
                        <!--Default Portfolio Item-->
                        <div class="default-portfolio-item">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box"><img src="images/paypal.jpg" alt=""></figure>
                                <div>
                                    <div class="inner-content">
                                        <div class="content">
                                            <h3><a href="#">Donate Via PayPal</a></h3>
                                            <a class="arrow lightbox-image" href="images/gallery/image-9.jpg"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column masonry-column all scholarship education col-md-6 col-sm-6 col-xs-12">
                        <!--Default Portfolio Item-->
                        <div class="default-portfolio-item">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box"><img src="images/paytm.jpg" alt=""></figure>
                                <div>
                                    <div class="inner-content">
                                        <div class="content">
                                            <h3><a href="#">Donate Via Paytm</a></h3>
                                            <a class="arrow lightbox-image" href="images/gallery/image-9.jpg"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                


                    <form class="form-horizontal">
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="name">Name:</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="name" placeholder="Enter Name">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-2" for="father">Fathers Name:</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="father" placeholder="Enter Father Name">
                        </div>
                      </div>  

                      <div class="form-group">
                        <label class="control-label col-sm-2" for="address">Address:</label>
                        <div class="col-sm-10"> 
                          <input type="text" class="form-control" id="address" placeholder="Enter Address">
                        </div>
                      </div>   
                      <div class="form-group">
                        <label class="control-label col-sm-2" for="donate-method">Address:</label>
                        <div class="col-sm-10"> 
                          <select class="form-control" id="donate-method">
                            <option value="paytm">PayTm</option>
                            <option value="paypal">PayPal</option>                            
                          </select>
                        </div>
                      </div>                                          
                      <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                      </div>
                    </form>
                    
                </div>
                
            </div>
        </section>
        
<?php include 'footer.php'; ?>  
        
      