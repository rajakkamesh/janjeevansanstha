 <!--Main Footer-->
    <footer class="main-footer" style="background-image:url(images/background/footer-bg.jpg);">
    	
        <!--Footer Upper-->        
        <div class="footer-upper">
            <div class="auto-container">
                <div class="row clearfix">
                	
                    <!--Two 4th column-->
                    <div class="col-md-6 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                            <div class="col-lg-6 col-sm-6 col-xs-12 column">
                            	<h2>Information</h2>
                                <div class="footer-widget about-widget">
                                	<div class="text">
                                        <h3 style="color: #f25f43">Jan Jeevan Sanstha</h3>
                                    </div>
                                    <br>
                                    
                                    <ul class="contact-info">
                                    	<li><span class="icon fa fa-map-marker"></span> Village- khushalgarhi, Post-Talivnagar, Aligarh, U.P.-202127</li>
                                        <li><span class="icon fa fa-envelope-o"></span>  info@janjeevansanstha.com</li>
                                        <li><span class="icon fa fa-phone"></span> +91 8006762604</li>
                                        
                                    </ul>
                                    
                                </div>
                            </div>
                            
                            <!--Footer Column-->
                            <div class="col-lg-6 col-sm-6 col-xs-12 column">
                                <h2> Photos</h2>
                                <div class="footer-widget flickr-widget">
                                    
                                    <div class="thumbs-outer clearfix">
                                    	<figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-1.jpg" ><img src="images/gallery/flickr-thumb-1.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-2.jpg" ><img src="images/gallery/flickr-thumb-2.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-3.jpg"><img src="images/gallery/flickr-thumb-3.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-4.jpg"><img src="images/gallery/flickr-thumb-4.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-1.jpg"><img src="images/gallery/flickr-thumb-5.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-2.jpg"><img src="images/gallery/flickr-thumb-6.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-3.jpg"><img src="images/gallery/flickr-thumb-7.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-4.jpg"><img src="images/gallery/flickr-thumb-8.png" alt=""></a></figure>
                                        <figure class="image-thumb"><a class="lightbox-image" href="images/shop/image-1.jpg"><img src="images/gallery/flickr-thumb-9.png" alt=""></a></figure>
                                    </div>
        
                                </div>
                            </div>
                    	</div>
                    </div><!--Two 4th column End-->
                    
                    <!--Two 4th column-->
                    <div class="col-md-6 col-sm-12 col-xs-12" id="map" style="height: 350px;">
                    	
                    </div><!--Two 4th column End-->
                    
                </div>
                
            </div>
        </div>
        
        <!--Footer Bottom-->
    	<div class="footer-bottom">
            <div class="auto-container clearfix">
                <!--Copyright-->
                <div class="copyright text-center"><span class="theme_color">JAN JEEVAN SANSTHA</span> &copy; 2017</a></div>
            </div>
        </div>
        
    </footer>  
        
    
	</div><!--Page Wrapper End-->
</div>
<!--page-outer-container-->


<script>
      function initMap() {
        var aligarh = {lat: 27.8974, lng: 78.0880};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: aligarh
        });
        var marker = new google.maps.Marker({
          position: aligarh,
          map: map
        });
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh8qtBfkcIAeJ-yC4aIl3ki_Adt7u57Cg&callback=initMap">
    </script>



<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/revolution.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/bxslider.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>
</body>

<!-- Mirrored from alone.themexriver.com/alone/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Sep 2017 17:39:50 GMT -->
</html>