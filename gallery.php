<?php include 'header.php'; ?>  
        
        <!-- Hidden Bar -->
        <section class="hidden-bar right-align">
            
            <div class="hidden-bar-closer">
                <button class="btn"><i class="fa fa-close"></i></button>
            </div>
            <!-- Hidden Bar Wrapper -->
            <div class="hidden-bar-wrapper">
            
                <!-- .logo -->
                <div class="logo text-center">
                    <a href="index-2.html"><img src="images/logo-2.png" alt=""></a>			
                </div><!-- /.logo -->
                
                <!-- .Side-menu -->
                <div class="side-menu">
                <!-- .navigation -->
                    <ul class="navigation">
                        <li class="current dropdown"><a href="index-2.html">Home</a>
                            <ul class="submenu">
                                <li><a href="index-2.html">Home Page Style I</a></li>
                                <li><a href="index-3.html">Home Page Style II</a></li>
                                <li><a href="index-4.html">Home Page Style III</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="causes.html">Causes</a>
                            <ul class="submenu">
                                <li><a href="causes.html">Our Causes</a></li>
                                <li><a href="cause-single.html">Causes Detail</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="gallery-style-one.html">Gallery</a>
                            <ul class="submenu">
                                <li><a href="gallery-style-one.html">Gallery Style I</a></li>
                                <li><a href="gallery-style-two.html">Gallery Style II</a></li>
                                <li><a href="gallery-style-three.html">Gallery Style III</a></li>
                                <li><a href="gallery-style-four.html">Gallery Style IV</a></li>
                                <li><a href="gallery-style-five.html">Gallery Style V</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Pages</a>
                            <ul class="submenu">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-services.html">Our Services</a></li>
                                <li><a href="blog-list.html">Blog List View</a></li>
                                <li><a href="blog-grid.html">Blog Grid View</a></li>
                                <li><a href="blog-single.html">Blog Details</a></li>
                                <li><a href="our-stories.html">Welfare Stories</a></li>
                                <li><a href="event-calendar.html">Event Calendar</a></li>
                                <li><a href="event-details.html">Event Details</a></li>
                                <li><a href="error-page.html">404 Error Page</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="shop.html">Shop</a>
                            <ul class="submenu">
                                <li><a href="shop.html">Shop</a></li>
                                <li><a href="shop-single.html">Shop Details</a></li>
                                <li><a href="shopping-cart.html">Shopping Cart</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div><!-- /.Side-menu -->
            
                <div class="social-icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            
            </div><!-- / Hidden Bar Wrapper -->
        </section><!-- / Hidden Bar -->
        
        
        <!--Page Title Section-->
        <section class="page-title-section" style="background-image:url(images/background/page-title-1.jpg);">
            <div class="auto-container">
            	<div class="page-title">
                	
                    <h2>Gallery Style</h2>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                    	<!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Gallery</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!--Gallery Section-->
        <section class="gallery-section full-width-gallery">
        	<div class="auto-container">
                
                <!--Filter-->
                <div class="filters text-center">
                    <ul class="filter-tabs filter-btns clearfix anim-3-all">
                        <li class="active filter" data-role="button" data-filter="all">All</li>
                        <li class="filter" data-role="button" data-filter=".causes">FOOD</li>
                        <li class="filter" data-role="button" data-filter=".volunteer">EDUCATION</li>
                        <li class="filter" data-role="button" data-filter=".education">SAI BABA KIRTAN</li>
                        <li class="filter" data-role="button" data-filter=".stories">Stories</li>
                    </ul>
                </div>
         	</div>
         
            <!--Filter List-->
            <div class="filter-list clearfix">
                
                <!--Column-->
                <div class="column mix mix_all education stories col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <!--Default Portfolio Item-->
                    <div class="default-portfolio-item">
                        <div class="inner-box text-center">
                            <!--Image Box-->
                            <figure class="image-box"><img src="images/gallery/image-18.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="inner-content">
                                    <div class="content">
                                        <h1 style="color: #f25f43">"JAN JEEVAN SANSTHA"</h1>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Column-->
                <div class="column mix mix_all causes volunteer education stories col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <!--Default Portfolio Item-->
                    <div class="default-portfolio-item">
                        <div class="inner-box text-center">
                            <!--Image Box-->
                            <figure class="image-box"><img src="images/gallery/image-19.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="inner-content">
                                    <div class="content">
                                      <h1 style="color: #f25f43">"JAN JEEVAN SANSTHA"</h1>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <!--Column-->
                <div class="column mix mix_all causes volunteer col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <!--Default Portfolio Item-->
                    <div class="default-portfolio-item">
                        <div class="inner-box text-center">
                            <!--Image Box-->
                            <figure class="image-box"><img src="images/gallery/image-20.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="inner-content">
                                    <div class="content">
                                     <h1 style="color: #f25f43">"JAN JEEVAN SANSTHA"</h1>
                                       
            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Column-->
                <div class="column mix mix_all causes stories education col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <!--Default Portfolio Item-->
                    <div class="default-portfolio-item">
                        <div class="inner-box text-center">
                            <!--Image Box-->
                            <figure class="image-box"><img src="images/gallery/image-21.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="inner-content">
                                    <div class="content">
                                   <h1 style="color: #f25f43">"JAN JEEVAN SANSTHA"</h1>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Column-->
                <div class="column mix mix_all causes volunteer col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <!--Default Portfolio Item-->
                    <div class="default-portfolio-item">
                        <div class="inner-box text-center">
                            <!--Image Box-->
                            <figure class="image-box"><img src="images/gallery/image-22.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="inner-content">
                                    <div class="content">
                                      <h1 style="color: #f25f43">"JAN JEEVAN SANSTHA"</h1>
                                
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--Column-->
                <div class="column mix mix_all causes stories col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <!--Default Portfolio Item-->
                    <div class="default-portfolio-item">
                        <div class="inner-box text-center">
                            <!--Image Box-->
                            <figure class="image-box"><img src="images/gallery/image-23.jpg" alt=""></figure>
                            <div class="overlay-box">
                                <div class="inner-content">
                                    <div class="content">
                                      <h1 style="color: #f25f43">"JAN JEEVAN SANSTHA"</h1>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </section>
        
        
            
        <!--Main Footer-->
      <?php include 'footer.php'; ?>        



<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="fa fa-long-arrow-up"></span></div>

<script src="js/jquery.js"></script> 
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>
<script src="js/mixitup.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>
</body>

<!-- Mirrored from alone.themexriver.com/alone/gallery-style-five.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Sep 2017 17:44:59 GMT -->
</html>
