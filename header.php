<!DOCTYPE html>
<html>

<!-- Mirrored from alone.themexriver.com/alone/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 14 Sep 2017 17:30:51 GMT -->
<head>
<meta charset="utf-8">
<title>Jan jeevan Sanstha </title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/revolution-slider.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<!-- Responsive -->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->

<script type="text/javascript">
    var blink_speed = 500; var t = setInterval(function () { 
        var ele = document.getElementById('blinker');
        ele.style.visibility = (ele.style.visibility == 'hidden' ? '' : 'hidden'); }, blink_speed);
</script>
</head>

<!--Use "boxed-layout" class with body to See Boxed View-->
<body>
<div class="page-outer-container">
	<div class="page-wrapper">
 	
        <!-- Preloader -->
        <div class="preloader"></div>
        
        <!-- Main Header -->
        <header class="main-header">
            <!-- Top Bar -->
            <div class="top-bar">
                <div class="auto-container clearfix">
                    <!-- Top Left -->
                    <div class="top-left">
                        <ul class="clearfix" style="text-align: left;">
                            <li><a href="mailto:admin@alone.com"><span style="color:black;font-size: 24px;" class="fa fa-envelope"></span> info@janjeevansanstha.com</a></li>
                            <li><a href="#"><span style="color:black;font-size: 24px;" class="glyphicon glyphicon-phone-alt"></span> 
                            &nbsp;&nbsp;+91-7906740833</a></li>
                        </ul>
                    </div>
                    
                    <!-- Top Right -->
                    <div class="top-right clearfix">
                        <!--Right Nav-->
                        <ul class="right-nav clearfix">
                            <li><img src="images/icons/instagram.jpg" style="width: 30px;border-radius: 26px;box-shadow: 0px 0px 10px 1px #ffffff;" /></li>
                            <li><img src="images/icons/facebook.jpg" style="width: 30px;border-radius: 26px;box-shadow: 0px 0px 10px 1px #ffffff;" /></li>
                            <li><img src="images/icons/twitter.jpg" style="width: 30px;border-radius: 26px;box-shadow: 0px 0px 10px 1px #ffffff;" /></li>                       
                            <li><img src="images/icons/google.jpg" style="width: 30px;border-radius: 26px;box-shadow: 0px 0px 10px 1px #ffffff;" /></li>                        
                        </ul>
                        
                        <!--Donate Btn-->
                        <div class="donate-outer">
                           <!--  <a href="#"><img src="images/icons/paypal.png" style="width: 148px;" alt="PayPal"> -->
                                <!-- <a href="#"><img src="images/icons/paytm.jpg" style="width: 148px;" alt="PayPal"> -->
                        
                            <!-- <a id="blinker" href="#" class="theme-btn btn-donate"><b style="font-size: 100%; font-size: 130%;">Donate Now</b></a> -->
                        <a id="blinker" href="donate.php" class="theme-btn btn-style-one" style="transition: all 0.2s ease-out 0s; min-height: 0px; min-width: 0px; line-height: 29px; border-width: 2px; margin: 0px; padding: 14px 35px; letter-spacing: 0px; font-size: 13px; background-color: #000000">Donate Now</a>
</a>
                        </div>
                        
                    </div>
                    
                </div>
            </div><!-- Top Bar End -->
            
            <!-- Lower Section -->
            <div class="lower-section">
                <div class="auto-container">
                    
                    <!--Outer Box-->
                    <div class="outer-box clearfix">
                    
                        <!-- Logo -->
                        <div class="logo">
                            <a href="index.php"><img src="images/logo.png" alt="Alone" title="Alone"></a>
                         </div>
                         
                         <div class="clearfix hidden"></div>
                         
                        <!-- Hidden Nav Toggler -->
                        <div class="nav-toggler">
                        <button class="hidden-bar-opener"><span class="icon fa fa-bars"></span></button>
                        </div><!-- / Hidden Nav Toggler -->
                
                
                        
                             <!--Product Item-->
                                
                                
            
                            </div>
                        </div>
                        
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            
                            <div class="navbar-header">
                                <!-- Toggle Button -->    	
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            
                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation">
                                    <li class="current dropdown"><a href="index.php">Home</a>
                                        </li>
                                    <li class="dropdown"><a href="about.php">About</a>
                                       <!--  <ul>
                                            <li><a href="#">Our Causes</a></li>
                                            <li><a href="#">Causes Detail</a></li>
                                        </ul> -->
                                    </li>

                                     <li class="dropdown"><a href="work.php">Work</a>


                                        <!--  <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-services.html">Our Services</a></li>
                                <li><a href="blog-list.html">Blog List View</a></li>
                                <li><a href="blog-grid.html">Blog Grid View</a></li>
                                <li><a href="blog-single.html">Blog Details</a></li>
                                <li><a href="our-stories.html">Welfare Stories</a></li>
                                <li><a href="event-calendar.html">Event Calendar</a></li>
                                <li><a href="event-details.html">Event Details</a></li>
                                <li><a href="error-page.html">404 Error Page</a></li> -->
                                       
                                    </li>
                                    <li class="dropdown"><a href="gallery.php">Gallery</a>
                                       
                                    </li>

                                    <li class="dropdown"><a href="team.php">Team</a>
                                       
                                    </li>
                                    <li><a href="#">Join Us</a></li>
                                    <!-- <li><a href="join_us.php">Join Us</a></li> -->
                                    <li><a href="contact.php">Contact</a></li>

                                </ul>
                            </div>
                        </nav><!-- Main Menu End-->
                        
                    </div>
                </div>
            </div><!-- Lower Section End-->
            
        </header><!--End Main Header -->
        
        
        <!-- Hidden Bar -->
        <section class="hidden-bar right-align">
            
            <div class="hidden-bar-closer">
                <button class="btn"><i class="fa fa-close"></i></button>
            </div>
            <!-- Hidden Bar Wrapper -->
            <div class="hidden-bar-wrapper">
            
                <!-- .logo -->
                <div class="logo text-center">
                    <a href="index.php"><img src="images/logo-2.png" alt=""></a>			
                </div><!-- /.logo -->
                
                <!-- .Side-menu -->
                <div class="side-menu">
                <!-- .navigation -->
                    <ul class="navigation">
                        <li class="current dropdown"><a href="index.php">Home</a></li>
                        <li class="dropdown"><a href="causes.html">Causes</a>
                            <ul class="submenu">
                                <li><a href="causes.html">Our Causes</a></li>
                                <li><a href="cause-single.html">Causes Detail</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="gallery-style-one.html">Gallery</a>
                            <ul class="submenu">
                                <li><a href="gallery-style-one.html">Gallery Style I</a></li>
                                <li><a href="gallery-style-two.html">Gallery Style II</a></li>
                                <li><a href="gallery-style-three.html">Gallery Style III</a></li>
                                <li><a href="gallery-style-four.html">Gallery Style IV</a></li>
                                <li><a href="gallery-style-five.html">Gallery Style V</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Pages</a>
                            <ul class="submenu">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-services.html">Our Services</a></li>
                                <li><a href="blog-list.html">Blog List View</a></li>
                                <li><a href="blog-grid.html">Blog Grid View</a></li>
                                <li><a href="blog-single.html">Blog Details</a></li>
                                <li><a href="our-stories.html">Welfare Stories</a></li>
                                <li><a href="event-calendar.html">Event Calendar</a></li>
                                <li><a href="event-details.html">Event Details</a></li>
                                <li><a href="error-page.html">404 Error Page</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="shop.html">Shop</a>
                            <ul class="submenu">
                                <li><a href="shop.html">Shop</a></li>
                                <li><a href="shop-single.html">Shop Details</a></li>
                                <li><a href="shopping-cart.html">Shopping Cart</a></li>
                            </ul>
                        </li>
                        <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div><!-- /.Side-menu -->
            
                <div class="social-icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            
            </div><!-- / Hidden Bar Wrapper -->
        </section><!-- / Hidden Bar -->