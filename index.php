<?php include 'header.php'; ?>        

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Trigger the modal with a button -->
<button style="display: none;" id="modal-button" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      

    </div>

  </div>
</div>


        <!--Main Slider-->
        <section class="main-slider default-slider">
            
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>

                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/4.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="images/main-slider/4.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-160"
                        data-speed="1500"
                        data-start="500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h2 class="bg-theme">WE ORGANIZE</h2></div>
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-80"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h1 class="text-uppercase"><span class="theme_color">SAI SANDHYA</span> SAI KIRTAN</h1></div>
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="10"
                        data-speed="1500"
                        data-start="1500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h3 class="text text-center">Sai Sandhya bhajan is a form of religious ritual done in order to show devotion towards Sai Baba. For a true devotee</h3></div>
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="120"
                        data-speed="1500"
                        data-start="2000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="#" class="theme-btn btn-style-one">Join Our Compain</a></div>
                        
                        
                        </li>                         
                        
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/1.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="images/main-slider/1.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-160"
                        data-speed="1500"
                        data-start="500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h2 class="bg-theme">WE SUPPORT</h2></div>
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-80"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h1 class="text-uppercase"><span class="theme_color">ACCOMMODATE</span> FEED AND EDUCATE</h></div>
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="10"
                        data-speed="1500"
                        data-start="1500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h4 class="text text-center">We as NGO to serve and support for accomodate, feed &amp; educate helpless children via opening or Supporting Hostels, Library, Medical centres etc</h4></div>
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="120"
                        data-speed="1500"
                        data-start="2000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="#" class="theme-btn btn-style-one">Join Our Compain</a></div>
                        
                        
                        </li>
                        
                        
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/2.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="images/main-slider/2.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-150"
                        data-speed="1500"
                        data-start="500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h2>WE SUPPORT</h2></div>
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-70"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h1 class="text-uppercase"><span class="theme_color">DIGITAL EDUCATION </span>TO EVERY CHILD</h1></div>
                        
                      
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="60"
                        data-speed="1500"
                        data-start="1500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h4 class="text text-center">Promote Follow Central and State Govt. schemes to spread digital Literacy to every child especially<br> who can't afford even education as they are future of Our India</h4></div>
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="160"
                        data-speed="1500"
                        data-start="1500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="#" class="theme-btn btn-style-one">Join Our Campaign</a></div>
                        
                        
                        </li>


                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1000" data-thumb="images/main-slider/3.jpg"  data-saveperformance="off"  data-title="Awesome Title Here">
                        <img src="images/main-slider/3.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                        
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-150"
                        data-speed="1500"
                        data-start="500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h2>WE SUPPORT</h2></div>
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="-70"
                        data-speed="1500"
                        data-start="1000"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h1 class="text-uppercase"><span class="theme_color"> CLEAN WATER, </span>SAVE WATER</h1></div>
                        
                  
                        
                        <div class="tp-caption sfr sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="60"
                        data-speed="1500"
                        data-start="1500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><h4 class="text text-center">One of our Prime objective is to provide Clean Water and easily accessibility for All. <br>for this we install and Manage water Pumps, set up water tank and Purifier</h4></div>
                        
                        <div class="tp-caption sfl sfb tp-resizeme"
                        data-x="center" data-hoffset="15"
                        data-y="center" data-voffset="160"
                        data-speed="1500"
                        data-start="1500"
                        data-easing="easeOutExpo"
                        data-splitin="none"
                        data-splitout="none"
                        data-elementdelay="0.01"
                        data-endelementdelay="0.3"
                        data-endspeed="1200"
                        data-endeasing="Power4.easeIn"
                        style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><a href="#" class="theme-btn btn-style-one">Join Our Campaign</a></div>
                        
                        
                        </li>

                        
                        
                        
                    </ul>
                    
                </div>
            </div>
        </section>
        
        
        <!--Normal Section-->
        <section class="normal-section">
            <div class="auto-container">
                  <div class="row clearfix">
                  
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <!--Vertical Green Bar-->
                        <div class="vertical-green-bar">
                              <!--Upper Part-->
                              <div class="upper-part">
                              <h2 style="color: #ffffff">Jan <br><span class="big-text" style="color: #ffffff!important;">Jeevan</span> <br><span class="white-text" style="color: #fffffff">Sansthan</span></h2>
                                
                                <!--Donation Box-->
                                <div class="donation-box">
                                    <div class="title-text" style="text-transform: none; size: 12%">has been working in U.P.for over 15 Months. Eending poverty and social injustice. We do this through well-planned and comprehensive programmes in health,education, livelihoods and disaster preparedness and response.</div>
                                   <!--  <div class="needed-amount">391,020</div> -->
                                    
                                    <div class="donation-bar-outer">
                                          
                                        <!-- <div class="raised-percent">100%</div> -->
                                          <div class="donation-bar">
                                          <div class="bar-fill" style="width:100%;"></div>
                                        </div>
                                    </div>
                                    
                                  
                                </div>
                                
                            </div>
                            
                              
                            <!--Donate Link Box-->
                            <div class="donate-link-box" style="background-image:url(images/background/green-bar-bg.jpg);">
                              <a href="#" class="theme-btn">Donate Now</a>
                            </div>
                                
                        </div>
                    </div>
                    
                    
                    <!--Become Volunteer-->
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <!--Outer-->
                        <div class="become-volunteer-outer wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                              <div class="become-volunteer-column" style="background-image:url(images/background/image-1.jpg);">
                              <div class="inner-box">
                                    <div class="overlay-box">
                                        <div class="content-box">
                                            <h4 class="text-center">We Need You!!  And Your Help</h4>
                                            <h2 class="text-center">Become <span class="theme_color">Volunteer</span></h2>
                                            <div class="lower-content">
                                                <div class="desc-text">To Serve Humanity, We need your Support and Blessing in form of Contribution. We highly admire and appreciate your Contributions to Fulfill our Objective and make our Uttar Pradesh a Place to feel Proud for.</div>
                                                <a href="#" class="theme-btn btn-style-one apply-btn">Apply Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                
                </div>
            </div>
        </section>
        
        
        <!--Recent Causes Section-->
        <section class="recent-causes-section">
            <div class="auto-container">
                  <!--Section Title-->
                <div class="section-title">
                  
                    <h2 style="color:#f25f43 ">Our Causes</h2>
                    
                </div>
                
                  <div class="row clearfix">
                        

                        <!--Column-->
                    <div class="column col-md-12 col-sm-12 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                              <div class="inner-box text-center">
                              <!--Image Box-->
                              <figure class="image-box">
                                    <img src="images/resource/1.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">

 <h1 style="color: #f25f43">Jan Jeevan Sanstha</h1> <h4 style="color: #ffffff"> has been launched to shri Sathya Sai Baba <br><h3>Kirtan, Sandhya and Jagran</h3><h1 style="color: #f25f43 ">Free Of Cost!!</h1></h4>
                                           
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    <div class="cause-over-title"><span class="raised-money" style="color: #ffffff">We organize Shri Shirdi Sai Baba Kirtan, Sandhya and Jagran <h3 style="color: #f25f43">"Free Of Cost"</h3></span></div>
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                          <div class="bar-fill" style="width:100%;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3  class="cause-title"><a style="color: #f25f43" href="#"> SHRI SHIRDI SAIBABA KIRTAN, SANDHYA</a></h3>                               
                                
                            </div>
                        </div>
                    </div>    
                                   
                    <!--Column-->
                    <div class="column col-md-3 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                              <div class="inner-box text-center">
                              <!--Image Box-->
                              <figure class="image-box">
                                    <img src="images/resource/cause-image-1.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content"> 
                                          
                                           <h3 style="color: #f25f43"> Jan Jeevan Sanstha</h3>
                                           
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    <div class="cause-over-title"><span class="raised-money" style="color: #ffffff">We work to end hunger and malnutrition in India </span></div>
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                          <div class="bar-fill" style="width:100%;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3  class="cause-title"><a style="color: #f25f43" href="#">FOOD</a></h3>                               
                                
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column col-md-3 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/cause-image-2.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                          
                                           <h3 style="color: #f25f43"> Jan Jeevan Sanstha</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    <div class="cause-over-title"><span class="raised-money" style="color: #ffffff">We believes in helping  individuals live a life </span>
                                    </div>
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a  style="color: #f25f43" href="#">ACCOMODATION</a></h3>                               
                                
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column col-md-3 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/cause-image-3.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                           <h3 style="color: #f25f43"> Jan Jeevan Sanstha</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    <div class="cause-over-title"><span class="raised-money" style="color: #ffffff">Education is the key to empowering  women and girls </span>  </div>
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a style="color: #f25f43" href="#">EDUCATION</a></h3>                               
                                
                            </div>
                        </div>
                    </div>

                    <!--Column-->
                    <div class="column col-md-3 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column wow fadeInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/cause-image-4.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                           <h3 style="color: #f25f43"> Jan Jeevan Sanstha</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    <div class="cause-over-title"><span class="raised-money" style="color: #ffffff">We works towards providing immediate </span></div>
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a  style="color: #f25f43" href="#">AWARENESS</a></h3>                               
                                
                            </div>
                        </div>
                    </div>

                   




                    
                        </div>
            </div>
        </section>
        
        
    
    
      
        
        
        
        <!--Recent Donors Section-->
        <section class="recent-donors-section" style="background-image:url(images/background/recent-donors-bg.jpg);">
            <div class="auto-container">
                  <div class="row clearfix">
                  <div class="title-column col-lg-3 col-md-12 col-sm-12 col-xs-12">
                        <div class="inner-box">
                            <h2 style="color: #000000">Recent Donaters</h2>
                            <h5 class="desc-text">We need your continued support to enable us to continue with our work.</h5>

                        </div>

                    </div>
                    
                    <div class="carousel-column col-lg-9 col-md-12 col-sm-12 col-xs-12">
                        <div class="donors-header clearfix">
                              <h2 style="color: #f25f43" class="pull-left">Join Our Campaign: Donate</h2>
                            <div class="pull-right" "><a href="#" class="theme-btn btn-style-one">Join Now</a></div>
                        </div>
                        
                        <div class="slider-outer">
                              <ul class="donors-carousel">
                              <!--Donor Column-->
                                <li class="default-donor-column">
                                    <div class="inner-box">
                                          <figure class="donor-thumb"><img src="images/resource/donor-thumb-1.jpg" alt=""></figure>
                                        <h3>Ashish</h3>
                                        <h5>New Delhi</h5>
                                       <h4 class="raised-money"  style="color: #f25f43">Donated : 1,000</h4>
                                    </div>
                                </li>
                                <!--Donor Column-->
                                <li class="default-donor-column">
                                    <div class="inner-box">
                                          <figure class="donor-thumb"><img src="images/resource/donor-thumb-2.jpg" alt=""></figure>
                                        <h3>Kamesh</h3>
                                    <h5>Noida</h5>
                                        <h4 class="raised-money"  style="color: #f25f43">Donated : 500</h4>
                                    </div>
                                </li>
                                <!--Donor Column-->
                                <li class="default-donor-column">
                                    <div class="inner-box">
                                          <figure class="donor-thumb"><img src="images/resource/donor-thumb-3.jpg" alt=""></figure>
                                        <h3>Fahad</h3>
                                         <h5>Saharanpur</h5>
                                        
                                        <h4 class="raised-money" style="color: #f25f43">Donated : 1500</h4>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </section>
        
        
        <!--Custome Background Section-->
        <section class="custom-bg-section" style="background-image:url(images/background/image-3.jpg);">
            <div class="auto-container">
                  
                <div class="content-outer">
                  
                    <!--Slider Outer-->
                    <div class="slider-outer">
                        <div class="custom-slider">
                            <!--Slide Item-->
                            <article class="slide-item">
                                <div class="slide-content">
                                    <div class="day-remaining">Jan Jeevan Sanstha</div>
                                    <h2>WE HELP THOUSANDS OF Kids</h2>
                                    <h3>TO GET THEIR </h3>
                                    <h1 style="color: #f25f43"><b>"Food"</b></h1>
                                    <br>


                                    <div class="text" style="font-size: 20px">We work to end hunger and malnutrition in India by providing food to vulnerable people.A major portion of what we do is redistributing good extra food to people in need.
                                </div>
                                <!--Info Content-->
                               <!--  <div class="info-content clearfix">
                                    <div class="info-column"><div class="inner"><div class="content">November 19  -  November 26</div></div></div>
                                    <div class="info-column active"><div class="inner"><div class="content">130 Ohua ave. Honolulu<br>96815 US Saint USA</div></div></div>
                                    <div class="info-column"><div class="inner"><div class="content">11:55 pm - 11:59 pm </div></div></div>
                                </div> -->
                            </article>
                            
                            <!--Slide Item-->
                            <article class="slide-item">
                                <div class="slide-content">
                                    <div class="day-remaining">Jan Jeevan Sanstha</div>
                                    <h2>WE HELP THOUSANDS OF Kids</h2>
                                    <h3>TO GET THEIR </h3>
                                    <h1 style="color: #f25f43"><b>"ACCOMODATION"</b></h1>
                                    <br>

                                    
                                    <div class="text" style="font-size: 20px">We believes in helping individuals live a life of dignity. To achieve this, our livelihood programmes focuses on generating sustainable livelihoods. This is done through capacity building, fostering community links and promoting small businesses.</div>
                                </div>
                                <!--Info Content-->
                               <!--  <div class="info-content clearfix">
                                    <div class="info-column"><div class="inner"><div class="content">November 19  -  November 26</div></div></div>
                                    <div class="info-column active"><div class="inner"><div class="content">130 Ohua ave. Honolulu<br>96815 US Saint USA</div></div></div>
                                    <div class="info-column"><div class="inner"><div class="content">11:55 pm - 11:59 pm </div></div></div>
                                </div> -->
                            </article>
                            
                            <!--Slide Item-->
                            <article class="slide-item">
                                <div class="slide-content">
                                    <div class="day-remaining">Jan Jeevan Sanstha</div>
                                    <h2>WE HELP THOUSANDS OF Kids</h2>
                                    <h3>TO GET THEIR </h3>
                                     <h1 style="color: #f25f43"><b>"EDUCATION"</b></h1>
                                     <br>

                                    <div class="text" style="font-size: 20px">Education is the key to empowering women and girls, which helps bring about social equality. Girls’ education programme works on improving lives and providing opportunities for girls and women through increased participation in formal and alternative education systems.</div>
                                </div>
                                <!--Info Content-->
                                <!-- <div class="info-content clearfix">
                                    <div class="info-column"><div class="inner"><div class="content">November 19  -  November 26</div></div></div>
                                    <div class="info-column active"><div class="inner"><div class="content">130 Ohua ave. Honolulu<br>96815 US Saint USA</div></div></div>
                                    <div class="info-column"><div class="inner"><div class="content">11:55 pm - 11:59 pm </div></div></div>
                                </div> -->
                            </article>
                             <article class="slide-item">
                                <div class="slide-content">
                                    <div class="day-remaining">Jan Jeevan Sanstha</div>
                                    <h2>WE HELP THOUSANDS OF Kids</h2>
                                    <h3>TO GET THEIR </h3>
                                    <h1 style="color: #f25f43"><b>"AWARENESS"</b></h1>
                                    <br>
                                    <div class="text" style="font-size: 20px">We works towards providing immediate relief and assists in the rehabilitation process of the affected communities in the aftermath of any calamity. We help communities build their capacity to better cope with and recover from disasters. </div>
                                </div>
                                <!--Info Content-->
                                <!-- <div class="info-content clearfix">
                                    <div class="info-column"><div class="inner"><div class="content">November 19  -  November 26</div></div></div>
                                    <div class="info-column active"><div class="inner"><div class="content">130 Ohua ave. Honolulu<br>96815 US Saint USA</div></div></div>
                                    <div class="info-column"><div class="inner"><div class="content">11:55 pm - 11:59 pm </div></div></div>
                                </div> -->
                            </article>
                             <article class="slide-item">
                                <div class="slide-content">
                                    <div class="day-remaining">Jan Jeevan Sanstha</div>
                                    <h2>WE ORGRANIZE</h2>
                                    <h3>SHRI SAI BABA</h3>
                                    <h1 style="color: #f25f43"><b>"KIRTAN, SANDHYA"</b></h1>
                                    <br>
                                    <div class="text" style="font-size: 20px">Jan Jeevan Sanstha has been launched to shri Sathya Sai Baba Kirtan, Sandhya and Jagran <br><h2>Free Of Cost!!</h2> </div>
                                </div>
                                <!--Info Content-->
                                <!-- <div class="info-content clearfix">
                                    <div class="info-column"><div class="inner"><div class="content">November 19  -  November 26</div></div></div>
                                    <div class="info-column active"><div class="inner"><div class="content">130 Ohua ave. Honolulu<br>96815 US Saint USA</div></div></div>
                                    <div class="info-column"><div class="inner"><div class="content">11:55 pm - 11:59 pm </div></div></div>
                                </div> -->
                            </article>
                            
                        </div>
                    </div>
                    
                    <a href="#" class="theme-btn btn-style-three">DONATE NOW!!</a>
                    
                </div>
                
             <!--    <script type="text/javascript">
                      $( document ).ready(function() {
                            console.log( "ready!" );
                            $('#modal-button').click();
                         
                        });

                      setTimeout(function() {
                            $('#myModal').modal('hide');
                        }, 2000);

                      
                      
                </script> -->
                
                  
            </div>
        </section>
<?php include 'footer.php'; ?>          
        
        
       
