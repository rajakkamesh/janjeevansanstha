<?php include 'header.php'; ?>     
        
        
        <!--Page Title Section-->
        <section class="page-title-section" style="background-image:url(images/background/page-title-1.jpg);">
            <div class="auto-container">
            	<div class="page-title">
                	
                    <h1>Our Work</h1>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                    	<!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Our Work</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!--Our Blog Section / List View-->
        <section class="our-blog-section blog-list-view">
        	<div class="auto-container">
                
                <!--Blog Post-->
                <article class="default-blog-post">
            		<div class="inner-box">
                    	<div class="row clearfix">
                            
                            <!--Image Column-->
                            <div class="col-lg-8 col-md-8 col-xs-12 pull-right">
                            	<figure class="image-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms"><a href="#"><img src="images/main-slider/4.jpg" alt=""></a></figure>
                            </div>
                            
                        	<div class="col-lg-4 col-md-4 col-xs-12 pull-left">
                            	<!--Post Content-->
                            	<div class="post-content">
                                    <h1 style="color: #000000">we Organize</h1>
                                    <div class="post-title">
                                        <h2 style="color: #f25f43">SAI SANDHYA,KIRTAN and Jagran</h2>
                                    </div>
                                    <!--Post Info-->
                                    <ul class="post-info">
                                        <li><span class="icon fa fa-user" style="color: #f25f43"></span>By Jan Jeevan Sanstha</li>
                        
                                    </ul>
                                    
                                    <!--Post Text-->
                                    <div class="post-text">
 Jan Jeevan Sanstha has been launched to shri Sathya Sai Baba Kirtan, Sandhya and Jagran Free Of Cost!!</div>
                                    
                                    <a href="Contact.php" class="theme-btn btn-style-one">Contact Us</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                
                <!--Blog Post-->
                <article class="default-blog-post">
            		<div class="inner-box">
                    	<div class="row clearfix">
                            
                            <!--Image Column-->
                            <div class="col-lg-8 col-md-8 col-xs-12 pull-left">
                            	<figure class="image-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"><a href="#"><img src="images/main-slider/1.jpg" alt=""></a></figure>
                            </div>
                            
                        	<div class="col-lg-4 col-md-4 col-xs-12 pull-right">
                            	<!--Post Content-->
                            	<div class="post-content">
                                    <h1 style="color: #000000">we Support</h1>
                                 <div class="post-title">
                                        <h2 style="color: #f25f43">ACCOMMODATE FEED AND EDUCATE</h2>
                                    </div>
                                    <!--Post Info-->
                                    <ul class="post-info">
                                       <li><span class="icon fa fa-user" style="color: #f25f43"></span>By Jan Jeevan Sanstha</li>
                                       
                                    </ul>
                                    
                                    <!--Post Text-->
                                    <div class="post-text">We as NGO to serve and support for accomodate, feed educate helpless children via opening or Supporting Hostels, Library, Medical centres etc </div>
                                    
                                    <a href="Donate.php" class="theme-btn btn-style-one">Donate Now</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                
                
                <!--Blog Post-->
                <article class="default-blog-post">
            		<div class="inner-box">
                    	<div class="row clearfix">
                            
                            <!--Image Column-->
                            <div class="col-lg-8 col-md-8 col-xs-12 pull-right">
                            	<figure class="image-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms"><a href="#"><img src="images/main-slider/2.jpg" alt=""></a></figure>
                            </div>
                            
                        	<div class="col-lg-4 col-md-4 col-xs-12 pull-left">
                            	<!--Post Content-->
                            	<div class="post-content">
                                    <!--Post Date-->
                                    <h1 style="color: #000000">we Support</h1>
                                    <div class="post-title">
                                 <h2 style="color: #f25f43">DIGITAL EDUCATION TO EVERY CHILD</h2>
                                    </div>
                                    <!--Post Info-->
                                    <ul class="post-info">
                                       <li><span class="icon fa fa-user" style="color: #f25f43"></span>By Jan Jeevan Sanstha</li>
                        
                                    </ul>
                                    
                                    <!--Post Text-->
                                    <div class="post-text">Promote Follow Central and State Govt. schemes to spread digital Literacy to every child especially who can't afford even education as they are future of Our India.</div>
                                    <a href="donate.php" class="theme-btn btn-style-one">Donate now</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                 <article class="default-blog-post">
                    <div class="inner-box">
                        <div class="row clearfix">
                            
                            <!--Image Column-->
                            <div class="col-lg-8 col-md-8 col-xs-12 pull-left">
                                <figure class="image-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"><a href="#"><img src="images/main-slider/3.jpg" alt=""></a></figure>
                            </div>
                            
                            <div class="col-lg-4 col-md-4 col-xs-12 pull-right">
                                <!--Post Content-->
                                <div class="post-content">
                                    <h1 style="color: #000000">we Support</h1>
                                 <div class="post-title">
                                        <h2 style="color: #f25f43">CLEAN WATER,SAVE WATER</h2>
                                    </div>
                                    <!--Post Info-->
                                    <ul class="post-info">
                                       <li><span class="icon fa fa-user" style="color: #f25f43"></span>By Jan Jeevan Sanstha</li>
                                       
                                    </ul>
                                    
                                    <!--Post Text-->
                                    <div class="post-text">One of our Prime objective is to provide Clean Water and easily accessibility for All. for this we install and Manage water Pumps, set up water tank and Purifier</div>
                                    
                                    <a href="#" class="theme-btn btn-style-one">Donate Now</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                
                
                
                
        	</div>
        </section>
        
        
            
        <?php include 'footer.php'; ?>     