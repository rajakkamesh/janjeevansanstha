<?php include 'header.php'; ?>  
        <!-- Hidden Bar -->
        <section class="hidden-bar right-align">
            
            <div class="hidden-bar-closer">
                <button class="btn"><i class="fa fa-close"></i></button>
            </div>
            <!-- Hidden Bar Wrapper -->
            <div class="hidden-bar-wrapper">
            
                <!-- .logo -->
                <div class="logo text-center">
                    <a href="index-2.html"><img src="images/logo-2.png" alt=""></a>			
                </div><!-- /.logo -->
                
                <!-- .Side-menu -->
                <div class="side-menu">
                <!-- .navigation -->
                    <ul class="navigation">
                        <li class="current dropdown"><a href="index-2.html">Home</a>
                            <ul class="submenu">
                                <li><a href="index-2.html">Home Page Style I</a></li>
                                <li><a href="index-3.html">Home Page Style II</a></li>
                                <li><a href="index-4.html">Home Page Style III</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="causes.html">Causes</a>
                            <ul class="submenu">
                                <li><a href="causes.html">Our Team</a></li>
                                <li><a href="cause-single.html">Causes Detail</a></li>  
                            </ul>
                        </li>
                        <li class="dropdown"><a href="gallery-style-one.html">Gallery</a>
                            <ul class="submenu">
                                <li><a href="gallery-style-one.html">Gallery Style I</a></li>
                                <li><a href="gallery-style-two.html">Gallery Style II</a></li>
                                <li><a href="gallery-style-three.html">Gallery Style III</a></li>
                                <li><a href="gallery-style-four.html">Gallery Style IV</a></li>
                                <li><a href="gallery-style-five.html">Gallery Style V</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="#">Pages</a>
                            <ul class="submenu">
                                <li><a href="about-us.html">About Us</a></li>
                                <li><a href="our-services.html">Our Services</a></li>
                                <li><a href="blog-list.html">Blog List View</a></li>
                                <li><a href="blog-grid.html">Blog Grid View</a></li>
                                <li><a href="blog-single.html">Blog Details</a></li>
                                <li><a href="our-stories.html">Welfare Stories</a></li>
                                <li><a href="event-calendar.html">Event Calendar</a></li>
                                <li><a href="event-details.html">Event Details</a></li>
                                <li><a href="error-page.html">404 Error Page</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="shop.html">Shop</a>
                            <ul class="submenu">
                                <li><a href="shop.html">Shop</a></li>
                                <li><a href="shop-single.html">Shop Details</a></li>
                                <li><a href="shopping-cart.html">Shopping Cart</a></li>
                            </ul>
                        </li>
                        <li><a href="contact-us.html">Contact</a></li>
                    </ul>
                </div><!-- /.Side-menu -->
            
                <div class="social-icons">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            
            </div><!-- / Hidden Bar Wrapper -->
        </section><!-- / Hidden Bar -->
        
        
        <!--Page Title Section-->
        <section class="page-title-section" style="background-image:url(images/background/page-title-1.jpg);">
            <div class="auto-container">
            	<div class="page-title">
                	
                    <h2>Our Team</h2>
                </div>
                
                <div class="clearfix">
                    <div class="breadcrumb-outer pull-right">
                    	<!--Breadcrumb-->
                        <ol class="breadcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active">Our TEAM</li>
                        </ol>
                    </div>
                </div>
                
            </div>
        </section>
        
        
        <!--All Causes Section-->
        <section class="recent-causes-section all-causes-section">
        	<div class="auto-container">
                
            	<div class="row clearfix">
       				
                           <!--Column-->
                    <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/nannu-singh.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>President</b></h1>
                                            <h2>SHRI NANNU SINGH</h2>
                                            <h3>S/o Shri Naval Singh</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#">Shri Nannu Singh</a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                             <!--Column-->
                    <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/nawal-singh.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>Vice-President</b></h1>
                                            <h2>SHRI NAVAL SINGH</h2>
                                            <h3>S/o Shri Munshilal</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#">Shri Naval Singh</a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                     <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/shri-rajkumar.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>Secretary</b></h1>
                                            <h2>SHRI RAJKUMAR</h2>
                                            <h3>S/o Shri Rishipal</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#">Shri RAJKUMAR</a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                     <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/hemlata-devi.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>Treasurer</b></h1>
                                            <h2>Smt. Hemlata Devi</h2>
                                            <h3>W/o- Shri Nannu Singh</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#"> SMT. Hemlata Devi</a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                     <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/Shri-manoj-kumar.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>Member</b></h1>
                                            <h2>Shri Manoj Kumar</h2>
                                            <h3>S/o- Shri Vashudev</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#">Shri manoj kumar</a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                   
                    <!--Column-->
                    <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/Neeraj-kumar-kushwah.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>Member</b></h1>
                                            <h2>Shri Neeraj K Kushwaha </h2>
                                            <h3>S/o- Shri  Narendra pal Singh</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#">Shri Neeraj K Kushwaha </a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                     <div class="column col-md-4 col-sm-6 col-xs-12">
                        <!--Default Cause Column-->
                        <div class="default-cause-column">
                            <div class="inner-box text-center">
                                <!--Image Box-->
                                <figure class="image-box">
                                    <img src="images/resource/shri-amar-singh.jpg" alt="">
                                    <!--Overlay Box-->
                                    <div class="overlay-box">
                                        <div class="content">
                                            <h1 style="color: #dc632d "><b>Member</b></h1>
                                            <h2>Shri Amar Singh </h2>
                                            <h3>S/o- Shri Dorilal</h3>
                                        </div>
                                    </div>
                                    <!--Over Title-->
                                    
                                </figure>
                                
                                <!--Donation Bar-->
                                <div class="donation-bar-outer">
                                    <div class="donation-bar">
                                        <div class="bar-fill" style="width:100%; background-color:  #dc632d;"></div>
                                    </div>
                                    
                                </div>
                                
                                <h3 class="cause-title"><a href="#">Shri Amar Singh </a></h3>
                                
                                
                            </div>
                        </div>
                    </div>
                    <!--Column-->
                    
       			</div>
                
                
        	</div>
        </section>
        
    <?php include 'footer.php'; ?>  